FROM node:14

# Setting working directory. All the path will be relative to WORKDIR
ENV REACT_ROOT /usr/src/app/
WORKDIR /usr/src/app/

COPY ./package.json ./
COPY ./package-lock.json ./

# Installing dependencies
RUN npm install

# Copying source files
COPY . .
EXPOSE 3001

# Running the app
CMD ["npm", "run", "dev"]
