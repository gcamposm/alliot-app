# README Alliot-App

## Description
This project is created to complete a technical test for Alliot

## Software requirements

|                         |                      |
|-------------------------|----------------------|
| Programming language    | Node (14.16.1)       |

## Useful information

|                         |                      |
|-------------------------|----------------------|
| Server PORT             | 3001                 |

## General steps to run the project.
1. Fulfill the Software Requirements:
    - [Install Node](https://nodejs.org/en/download/).

2. Install the repository on your local directory [via GIT](https://docs.github.com/en/github/getting-started-with-github/getting-started-with-git/about-remote-repositories).

3. Create the `.env` file on the root of the repository with the required environment variables.

    >**Note**: this file may have already been created in the repository, so it shouldn't be needed in that case.

    - An example of environment variables:
    ```
    API_ENDPOINT=http://localhost:3000/v
    ```

4. Install the project's dependencies via npm.
    ```
    $ npm install
    ```

5. Run the server on port 3005
    ```
    $ npm run dev
    ```

6. You can log in at http://localhost:3001/login.
