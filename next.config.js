// /** @type {import('next').NextConfig} */
const withAntdLess = require('next-less');
const modifyVars = require('./src/assets/nextCustomVars.js');

const nextConfig = {
  publicRuntimeConfig: {
    NODE_ENV: process.env.NODE_ENV,
    API_ENDPOINT: process.env.API_ENDPOINT
  },
  modifyVars,
  lessVarsFilePath: './src/assets/base/variables.less',
  cssLoaderOptions: {},
  experimental: {
    newNextLinkBehavior: false,
  }
};

module.exports = withAntdLess(nextConfig);
