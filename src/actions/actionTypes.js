/* Network */
export const REQUEST = '[REQUEST]';
export const PENDING = 'API_PENDING';
export const FAILURE = 'API_FAILURE';
export const SUCCESS = 'API_SUCCESS';

/* API */
export const API_REQUEST = 'API_REQUEST';
export const API_PENDING = 'API_PENDING';
export const API_FAILURE = 'API_FAILURE';
export const API_SUCCESS = 'API_SUCCESS';

/* CRUD */
export const CREATE = 'CREATE';
export const READ = 'READ';
export const READ_SINGLE = 'READ_SINGLE';
export const UPDATE = 'UPDATE';
export const DELETE = 'DELETE';
export const DOWNLOAD = 'DOWNLOAD';

/* Modal */
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

/* Tasks */
export const TASKS = 'TASKS';
export const CLEAN_TASK = 'CLEAN_TASK';
export const SORT_TASKS = 'SORT_TASKS';
export const SET_TASK = 'SET_TASK';

/* Users */
export const SIGN_UP = 'SIGN_UP';
export const USER = 'USER';
export const CLEAN_USER_SERVICE = 'CLEAN_USER_SERVICE';
