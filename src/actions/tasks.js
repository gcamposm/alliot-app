import * as api from 'src/utils/api/tasks';
import { notification } from 'antd';
import {
  CREATE,
  UPDATE,
  READ,
  API_REQUEST,
  TASKS,
  SORT_TASKS,
  SET_TASK,
  DELETE
} from './actionTypes';

export const requestTasks =
  (filters = {}) =>
  dispatch => {
    const feature = `${TASKS} ${READ}`;

    dispatch({
      type: `${feature} ${API_REQUEST}`,
      meta: {
        feature,
        api: api.requestTasks(filters)
      }
    });
  };

export const createTask = (task, callback) => dispatch => {
  const feature = `${TASKS} ${CREATE}`;

  dispatch({
    type: `${feature} ${API_REQUEST}`,
    meta: {
      feature,
      api: api.createTask(task),
      notifications: {
        successMessage: 'Tarea creada con éxito'
      },
      failureCallback: res => {
        notification.error({ message: res.response.data.message, duration: 3 });
      },
      callback
    }
  });
};

export const editTask = (task, callback) => dispatch => {
  const feature = `${TASKS} ${UPDATE}`;

  dispatch({
    type: `${feature} ${API_REQUEST}`,
    meta: {
      feature,
      api: api.editTask(task),
      notifications: {
        successMessage: 'Tarea actualizada con éxito'
      },
      failureCallback: res => {
        notification.error({ message: res.response.data.message, duration: 3 });
      },
      callback
    }
  });
};

export const deleteTask = id => dispatch => {
  const feature = `${TASKS} ${DELETE}`;
  dispatch({
    type: `${feature} ${API_REQUEST}`,
    meta: {
      feature,
      api: api.deleteTask(id),
      notifications: {
        successMessage: 'Tarea eliminada'
      },
      callback: () => {
        dispatch(requestTasks());
      },
      failureCallback: () => {
        notification.error({ message: 'Error al eliminar tarea', duration: 3 });
      }
    }
  });
};

export const sortTasks = payload => dispatch => {
  dispatch({ type: SORT_TASKS, payload });
};

export const setTask = payload => dispatch => {
  dispatch({ type: SET_TASK, payload });
};
