import Router from 'next/router';
import { notification } from 'antd';

import {
  USER,
  READ,
  API_REQUEST,
  CLEAN_USER_SERVICE,
  SIGN_UP,
  CREATE
} from './actionTypes';
import * as api from '/src/utils/api/user';
import { setAuth, destroySession } from '/src/utils/auth';

export const login = form => dispatch => {
  const feature = `${USER} ${READ}`;
  dispatch({
    type: `${feature} ${API_REQUEST}`,
    meta: {
      api: api.login(form),
      feature,
      notifications: {
        successMessage: 'Has iniciado sesión'
      },
      callback: res => {
        setAuth(res.data.user, form);
        Router.push('/tasks');
      },
      failureCallback: res => {
        notification.error({ message: res.response.data.message, duration: 3 });
      }
    }
  });
};

export const logout = () => dispatch => {
  dispatch({ type: CLEAN_USER_SERVICE });
  destroySession();
  notification.success({ message: '¡Hasta luego!', duration: 5 });
};

export const createUser = (form, callback) => dispatch => {
  const feature = `${SIGN_UP} ${CREATE}`;

  dispatch({
    type: `${feature} ${API_REQUEST}`,
    meta: {
      api: api.signup(form),
      feature,
      callback: res => {
        setAuth(res.data.user, form);
        callback();
      },
      failureCallback: ({ response }) => {
        if (response && response.data) {
          notification.error({
            message: 'Error al crear usuario',
            description: response.data.message,
            duration: 0
          });
        }
      },
      notifications: {
        successMessage: 'Usuario creado!'
      }
    }
  });
};
