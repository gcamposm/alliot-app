const colorPrimary = '#282828';
const white = '#fff';
const grey = '#f7f9fa';
const textColor = '#333333';

module.exports = {
  'primary-color': colorPrimary,
  'card-padding-base': '15px',
  'layout-sider-background': white,
  'menu-bg': white,
  'border-radius-base': '2px',
  'collapse-content-bg': grey,
  'layout-header-background': colorPrimary,
  'heading-color': '#343434',
  'text-color': textColor,
  'label-color': '#000000a6',
  'layout-header-height': '50px',
  'form-item-margin-bottom': '20px',
  'form-vertical-label-padding': '0 0 4px',
  'table-header-color': 'rgba(0,0,0,0.65)',
  'tabs-vertical-margin': '0'
};
