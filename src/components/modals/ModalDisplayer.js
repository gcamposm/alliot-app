import { memo } from 'react';
import dynamic from 'next/dynamic';

import { ModalContainer } from './containers/ModalContainer';

const ModalTasks = dynamic(import('/src/components/tasks/modals/ModalTasks'), {
  loading: () => false
});

const MODAL_COMPONENTS = {
  TASKS: ModalTasks
};

const ModalDisplayer = () => {
  return (
    <ModalContainer>
      {({ hideModal, modalProps, modalType }) => {
        if (!modalType) {
          return null;
        }

        const SpecificModal = MODAL_COMPONENTS[modalType];
        if (!SpecificModal) {
          console.warn('Modal load error');
          return null;
        }

        return <SpecificModal {...modalProps} hideModal={hideModal} />;
      }}
    </ModalContainer>
  );
};

export default memo(ModalDisplayer);
