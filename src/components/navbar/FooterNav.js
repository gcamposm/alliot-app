import { Layout, Row, Col, Typography } from 'antd';
import moment from 'moment';

const { Footer } = Layout;
const { Text } = Typography;

const FooterNav = ({ style: { marginLeft } }) => {
  return (
    <Footer
      style={{
        textAlign: 'center',
        marginLeft,
        position: 'fixed',
        bottom: '0px',
        background: '#FFF',
        width: '100%',
        padding: '0.1rem 0px'
      }}
    >
      <Row type="flex" justify="start" gutter={24} >
        <Col span={10}>
          <Text type="secondary">
            {`Truck App © ${moment().year()}. Creado por Guillermo`}
          </Text>
        </Col>
      </Row>
    </Footer>
  );
};

export default FooterNav;
