import { Menu } from 'antd';
import Router, { withRouter } from 'next/router';
import Icon from '@ant-design/icons';
import { FaList, FaUser } from 'react-icons/fa';
import { logout } from '/src/actions/user';
import { useDispatch } from 'react-redux';
import styles from '/src/assets/main.modules.less';

const SideBar = ({ collapsed, router: { pathname } }) => {
  const rootPath = pathname.split('/')[1];
  const dispatch = useDispatch();

  const onMenuClick = ({ key }) => {
    switch (key) {
      case '/logout':
        dispatch(logout());
        break;

      default:
        Router.push(key);
        break;
    }
  };

  return (
    <Menu
      className={collapsed ? styles.menu_border_collpased : styles.menu_border}
      defaultSelectedKeys={['1']}
      mode="vertical"
      onClick={onMenuClick}
      selectedKeys={[`/${rootPath}`]}
    >
      <Menu.Item key="/tasks" >
        <Icon component={FaList} />
        <span>Tareas</span>
      </Menu.Item>
      <Menu.Item key="/logout" >
        <Icon component={FaUser} />
        <span>Cerrar Sesión</span>
      </Menu.Item>
    </Menu>
  );
};

export default withRouter(SideBar);
