import { Layout, Row, Col, Typography } from 'antd';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined
} from '@ant-design/icons';
import { withRouter } from 'next/router';

const { Header } = Layout;
const { Title } = Typography;

const colTopBarLeft = {
  xs: 12,
  sm: 12,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const TopBar = ({
  toggle,
  collapsed,
  router: { pathname },
  rightDrawer
}) => {

  const getPageTitle = () => {
    const paths = {
      tasks: 'Tareas',
      signup: 'Sign Up'
    };

    const rootPath = pathname.split('/')[1];

    return paths[rootPath];
  };

  const menuProps = {
    style: { fontSize: '16px' },
    onClick: toggle
  };

  return (
    <Header
      style={{
        background: '#f7f9fa',
        marginLeft: collapsed ? '80px' : '200px',
        padding: '0 20px'
      }}
    >
      <Row gutter={24}>
        <Col {...colTopBarLeft}>
          {collapsed ? <MenuUnfoldOutlined {...menuProps} /> : <MenuFoldOutlined {...menuProps} />}
          <Title level={4} style={{ marginLeft: '15px', display: 'inline-block' }}>
            {getPageTitle()}
          </Title>
        </Col>
      </Row>
      {rightDrawer ? <RightDrawer /> : null}
    </Header>
  );
};

export default withRouter(TopBar);
