import React, { memo } from 'react';
import Head from 'next/head';
import { Button } from 'antd';
import { withFormik } from 'formik';
import { Form, Checkbox } from 'formik-antd';

import { Email, FormInput } from '../library/FormikFormComponents';
import loginValidation from '../../utils/formValidations/loginValidation';
import { withUser } from './containers/UserContainer';
import UserWraper from '../signin/UserWrapper';

const Login = ({ loading }) => (
  <>
    <Head>
      <title>Iniciar Sesión</title>
    </Head>
    <UserWraper
        registerLink={false}
        marginTop="5%"
        leftOverride={{ width: '60%' }}
        rightOverride={{ width: '40%' }}
      >
      <Form layout="vertical">
        <Email />
        <FormInput type="password" name="password" placeholder="Contraseña" />
        <Button
          style={{ marginTop: '10px' }}
          loading={loading}
          data-testid="login"
          type="primary"
          htmlType="submit"
          block
        >
          Iniciar Sesión
        </Button>
        <Form.Item name="remember" style={{ marginTop: '15px', paddingBottom: '0px' }}>
          <Checkbox name="remember">Mantener sesión conectada</Checkbox>
        </Form.Item>
      </Form>
    </UserWraper>
  </>
);

export default withUser(
  withFormik({
    mapPropsToValues: () => ({ email: '', password: '', remember: true }),
    validationSchema: loginValidation,
    handleSubmit: (values, { props: { login } }) => {
      login(values);
    }
  })(memo(Login))
);
