import { memo, useState } from 'react';
import Link from 'next/link';
import styles from '../../assets/user/form.modules.less';

const UserWraper = ({
  children,
  marginTop = '20%',
  leftOverride = null
}) => {
  const [isLoading, setIsLoading] = useState(true);
  return (
    <>
      <div className={styles.table}>
        <div className={styles.table_cell_form} style={leftOverride}>
          <div className={styles.div_container} style={{ marginTop }}>
            <p>
              <Link href="/">
                <a>
                  <img src="../static/logo.svg" alt="logo" />
                </a>
              </Link>
            </p>
            {typeof children === 'function' ? children(isLoading) : children}
          </div>
        </div>
      </div>
    </>
  );
};

export default memo(UserWraper);
