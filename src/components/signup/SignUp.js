import Link from 'next/link';
import Head from 'next/head';
import { withRouter } from 'next/router';
import { Button, Row, Col } from 'antd';
import { Form } from 'formik-antd';
import { withFormik } from 'formik';
import {
  FormInput,
  Email
} from '../../components/library/FormikFormComponents';
import UserWraper from '../signin/UserWrapper';
import { withSignUp } from '../../components/signup/containers/SignUpContainer';

const col2ElementsProps = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
  xxl: 12
};

const col1ElementsProps = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
  xxl: 24
};

const SignUp = ({
  loading
}) => {

  return (
    <>
      <Head>
        <title>Regístrate</title>
      </Head>
      <UserWraper
        registerLink={false}
        marginTop="5%"
        leftOverride={{ width: '60%' }}
        rightOverride={{ width: '40%' }}
      >
        <Form>
          <Row type="flex" justify="space-between" gutter={16}>
            <Col {...col1ElementsProps}>
              <FormInput placeholder="Nombre" name="username" />
            </Col>
          </Row>
          <Email />
          <Row type="flex" justify="space-between" gutter={16}>
            <Col {...col2ElementsProps}>
              <FormInput name="password" placeholder="Contraseña" type="password" />
            </Col>
            <Col {...col2ElementsProps}>
              <FormInput
                name="password_confirmation"
                placeholder="Confirmar contraseña"
                type="password"
              />
            </Col>
          </Row>
          <Row>
            <Button type="primary" htmlType="submit" block loading={loading}>
              Crear Usuario
            </Button>
          </Row>
          <Row style={{ marginTop: '30px' }}>
            <Link href="/login">
              <a>Inicia sesión si ya tienes usuario</a>
            </Link>
          </Row>
        </Form>
      </UserWraper>
    </>
  );
};

export default withRouter(
  withSignUp(
    withFormik({
      mapPropsToValues: ({ signupForm }) => signupForm,
      handleSubmit: (values, { props: { createUser, router } }) => {
        const user = {
          ...values,
          email: values.email.toLowerCase(),
          username: values.username,
          password: values.password,
          password_confirmation: values.password_confirmation
        };

        createUser(user, () => {
          router.push('/tasks');
        });
      }
    })(SignUp)
  )
);
