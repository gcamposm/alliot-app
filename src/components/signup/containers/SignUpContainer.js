import { connect } from 'react-redux';
import { createUser } from '../../../actions/user';

const Container = props => {
  return props.children({ ...props });
};

const mapStateToProps = ({ user }) => {
  return { ...user };
};

const mapDispatchToProps = { createUser };

export const SignUpContainer = connect(mapStateToProps, mapDispatchToProps)(Container);

export const withSignUp = connect(mapStateToProps, mapDispatchToProps);
