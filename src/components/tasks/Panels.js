import { useEffect } from 'react';
import { withTasks } from '@/components/tasks/containers/TasksContainer';
import { Collapse } from 'antd';
import { Row, Col, Checkbox, Popconfirm } from 'antd';
import Icon from '@ant-design/icons';
import { FaTrash, FaPen } from 'react-icons/fa';

const { Panel } = Collapse;

const Tasks = ({ requestTasks, tasks, editTask, showModal, setTask, deleteTask }) => {
  useEffect(() => {
    requestTasks();
  }, []);

  const changeTaskCheck = task => {
    const updatedTask = task;
    updatedTask.completed = !task.completed;
    editTask(updatedTask);
  };

  const panelStyle = task => {
    if(task.completed) return {textDecoration: "line-through"}
    return {};
  };

  return (
    <>
      {tasks &&
        tasks.map(task => (
          <Row key={task.id} gutter={24} justify="start" align="middle">
            <Col span={1}>
              <Checkbox
                checked={task.completed}
                onClick={() => changeTaskCheck(task)}
              />
            </Col>
            <Col span={16}>
              <Collapse>
                <Panel header={task.title} key={task.id} style={panelStyle(task)}>
                  <p>{task.description}</p>
                </Panel>
              </Collapse>
            </Col>
            <Col span={1}>
            <a
              href=""
              onClick={e => {
                setTask(task);
                e.preventDefault();
                showModal('TASKS')
              }}
            >
              <Icon component={FaPen} />
            </a>
            </Col>
            <Col span={1}>
              <Popconfirm
                title="¿Seguro que quieres eliminar?"
                onConfirm={() => {
                  console.log('a eliminar tarea')
                  deleteTask(task.id);
                }}
                onCancel={() => {}}
                cancelText="No"
                okText="Sí, Eliminar"
              >
                <a
                  href=""
                  onClick={e => {
                    e.preventDefault();
                  }}
                >
                  <Icon style={{ color: 'red' }} component={FaTrash} />
                </a>
              </Popconfirm>
            </Col>
          </Row>
        ))}
    </>
  );
};

export default withTasks(Tasks);
