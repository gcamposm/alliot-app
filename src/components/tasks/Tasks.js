import { useState } from 'react';
import { withTasks } from '@/components/tasks/containers/TasksContainer';
import { Padding, Margin } from 'src/components/library';
import { Row, Col, Button } from 'antd';
import { FaPlus, FaSort } from 'react-icons/fa';
import Panels from './Panels';

const Tasks = ({ tasks, showModal, sortTasks }) => {
  const [order, setOrder] = useState('asc');
  const openTasksModal = () => {
    showModal('TASKS');
  };

  return (
    <>
      <Margin>
        <Padding>
          <>
          {tasks.lenght > 0 ? (
            <Row gutter={24} justify="start">
              <Col span={13}></Col>
              <Col span={4}>
                <Button
                  type="primary"
                  style={{ width: '100%' }}
                  onClick={() => {
                    sortTasks(order);
                    setOrder(order === 'asc' ? 'desc' : 'asc')
                  }}
                >
                  <FaSort />
                  <span>Ordenar</span>
                </Button>
              </Col>
            </Row>
          ) : null}
        </>
          <Panels tasks={tasks} />
          <Row gutter={24} justify="start">
            <Col span={1}></Col>
            <Col span={16}>
              <Button
                type="primary"
                style={{ width: '100%' }}
                onClick={() => {
                  openTasksModal();
                }}
              >
                <FaPlus />
                <span>... Agregar Tarea</span>
              </Button>
            </Col>
          </Row>
        </Padding>
      </Margin>
    </>
  );
};

export default withTasks(Tasks);
