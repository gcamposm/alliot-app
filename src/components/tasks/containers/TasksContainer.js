import { connect } from 'react-redux';
import * as actions from 'src/actions/tasks';
import { showModal, hideModal } from 'src/actions/modal';

const Container = props => {
  return props.children({ ...props });
};

const mapStateToProps = ({
  tasks,
}) => {
  return {
    ...tasks
  };
};

const mapDispatchToProps = {
  ...actions,
  showModal,
  hideModal
};

export const TasksContainer = connect(mapStateToProps, mapDispatchToProps)(Container);
export const withTasks = connect(mapStateToProps, mapDispatchToProps);
