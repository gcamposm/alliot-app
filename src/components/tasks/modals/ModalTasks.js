import { useEffect } from 'react';
import { Row, Col, notification, Spin } from 'antd';
import { Form, Input } from 'formik-antd';
import { withFormik } from 'formik';
import ReusableModal from 'src/components/modals/ReusableModal';
import { withTasks } from '../containers/TasksContainer';

const ModalTask = ({
  setValues,
  title,
  hideModal,
  handleSubmit,
  tasksLoading,
  task,
  deleteTask
}) => {
  const populateForm = () => {
    setValues({
      title: task.title,
      description: task.description
    });
  };

  useEffect(() => {
    if (task && task.id) populateForm();
  }, [task]);

  return (
    <ReusableModal
      title={title}
      clearAction={() => {
        hideModal();
      }}
      onSave={() => {
        handleSubmit();
      }}
      onDelete={() => {
        if (![null, undefined, ''].includes(task.id)) {
          deleteTask(task.id);
          hideModal();
        }
        return null;
      }}
      buttonLoading={tasksLoading}
    >
      <Spin spinning={tasksLoading}>
        <Form>
          <Row>
            <Col span={15} offset={1}>
              <Input
                name="title"
                placeholder="Título"
              />
            </Col>
          </Row>
          <Row>
            <Col span={15} offset={1}>
              <Input
                name="description"
                placeholder="Descripción"
              />
            </Col>
          </Row>
        </Form>
      </Spin>
    </ReusableModal>
  );
};

export default withTasks(
  withFormik({
    mapPropsToValues: () => ({
      title: '',
      description: ''
    }),
    handleSubmit: (
      values,
      {
        props: {
          task,
          onSuccessMessage,
          editTask,
          createTask,
          requestTasks,
          hideModal
        }
      }
    ) => {
      const validData = () => {
        const value = values.title && values.description;
        return value !== null && value !== '';
      };
      if (validData()) {
        const newTask = {
          task: {
            title: values.title,
            description: values.description
          }
        };
        if (task) {
          editTask({ ...newTask, id: task.id }, () => {
            hideModal();
            requestTasks();
            notification.success({ message: onSuccessMessage, duration: 3 });
          });
        } else {
          createTask(newTask, () => {
            hideModal();
            requestTasks();
            notification.success({ message: onSuccessMessage, duration: 3 });
          });
        }
      } else {
        notification.error({
          message: 'Complete los campos para guardar',
          duration: 3
        });
      }
    }
  })(ModalTask)
);
