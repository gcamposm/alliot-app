import React from 'react';
import Head from 'next/head';
import { Layout } from 'antd';
import { createWrapper } from 'next-redux-wrapper';
import 'antd/dist/antd.css';
import App from 'next/app';
import Router from 'next/router';
import NProgress from 'nprogress';
import { redirectIfNoAuth } from '/src/utils/auth';
import MainPage from '../../layouts/main';

import makeStore from '../store';

Router.events.on('routeChangeStart', () => {
  NProgress.start();
});

class AlliotTechnicall extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    const { pathname } = ctx;
    redirectIfNoAuth(ctx);
    return {
      pageProps,
      pathname
    };
  }
  render() {
    const {
      Component,
      pageProps,
      pathname
    } = this.props;
    return (
      <>
      <Head>
        <meta charSet="utf-8" />
        <link rel="icon" type="image/x-icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Alliot Technicall</title>
      </Head>
        <>
          {[
            '/login',
            '/signup'
          ].includes(pathname) ? (
            <Layout style={{ padding: '0 16px' }}>
              <Component {...pageProps} />
            </Layout>
          ) : (
              <MainPage>
                <Layout style={{ padding: '0 16px' }}>
                  <Component {...pageProps} />
                </Layout>
              </MainPage>
          )}
        </>
      </>
    );
  }
}

export default createWrapper(makeStore).withRedux(AlliotTechnicall);
