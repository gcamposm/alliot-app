import Login from '../components/signin/Login';
import UserPage from '../../layouts/user';

const Sign = props => {
  return (
    <UserPage {...props}>
      <Login />
    </UserPage>
  );
};

export default Sign;
