import Head from 'next/head';
import { Layout, Row, Col } from 'antd';
import SignUp from '../../components/signup/SignUp';
import styles from '../../assets/user/layout.modules.less';

const { Content } = Layout;

const SignUpPage = () => {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" key="viewport" />
        <meta charSet="utf-8" />
      </Head>
      <Layout className={styles.page_signin}>
        <Layout>
          <Content>
            <Row type="flex" style={{ alignItems: 'center' }}>
              <Col className={styles.form_box}>
                <Row type="flex" justify="center" align="middle">
                  <SignUp />
                </Row>
              </Col>
            </Row>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default SignUpPage;
