import Head from 'next/head';
import Tasks from '/src/components/tasks/Tasks.js';

const TasksPage = () => (
  <>
    <Head>
      <title>Tareas</title>
    </Head>
    <Tasks />
  </>
);

export default TasksPage;
