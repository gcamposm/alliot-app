import { combineReducers } from 'redux';

import modal from './modal';
import tasks from './tasks';
import user from './user';

export default combineReducers({
  modal,
  tasks,
  user
});
