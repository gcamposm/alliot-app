import {
  CREATE,
  READ,
  READ_SINGLE,
  UPDATE,
  DELETE,
  API_PENDING,
  API_FAILURE,
  API_SUCCESS,
  TASKS,
  CLEAN_TASK,
  SORT_TASKS,
  SET_TASK
} from '../actions/actionTypes';

const initialState = {
  tasks: [],
  task: null,
  tasksLoading: false
};

const sortTasks = (tasks, order) => {
  if(order === 'asc') return tasks.sort((a, b) => a.title.localeCompare(b.title));
  return tasks.sort((a, b) => b.title.localeCompare(a.title));
};

function tasks(state = initialState, action) {
  switch (action.type) {
    case `${TASKS} ${CREATE} ${API_PENDING}`:
    case `${TASKS} ${READ} ${API_PENDING}`:
    case `${TASKS} ${READ_SINGLE} ${API_PENDING}`:
    case `${TASKS} ${UPDATE} ${API_PENDING}`:
    case `${TASKS} ${DELETE} ${API_PENDING}`:
      return {
        ...state,
        tasksLoading: true
      };
    case `${TASKS} ${CREATE} ${API_FAILURE}`:
    case `${TASKS} ${READ} ${API_FAILURE}`:
    case `${TASKS} ${READ_SINGLE} ${API_FAILURE}`:
    case `${TASKS} ${UPDATE} ${API_FAILURE}`:
    case `${TASKS} ${DELETE} ${API_FAILURE}`:
      return {
        ...state,
        tasksLoading: false
      };
    case `${TASKS} ${CREATE} ${API_SUCCESS}`:
      return {
        ...state,
        tasks: [...state.tasks, action.payload],
        tasksLoading: false
      };
    case `${TASKS} ${READ} ${API_SUCCESS}`:
      return {
        ...state,
        tasksLoading: false,
        tasks: action.payload.tasks
      };
    case `${TASKS} ${READ_SINGLE} ${API_SUCCESS}`:
    case `${TASKS} ${UPDATE} ${API_SUCCESS}`:
      return {
        ...state,
        tasksLoading: false,
        task: action.payload
      };
    case `${TASKS} ${DELETE} ${API_SUCCESS}`:
      return {
        ...state,
        tasks: state.tasks.filter(task => task.id !== action.meta.id),
        tasksLoading: false
      };
    case CLEAN_TASK:
      return {
        ...state,
        task: null
      };
    case SORT_TASKS:
      return {
        ...state,
        tasks: [...sortTasks(state.tasks, action.payload)]
      };
    case SET_TASK:
      return {
        ...state,
        task: {...action.payload}
      };
    default:
      return { ...state };
  }
}

export default tasks;
