import {
  API_PENDING,
  API_FAILURE,
  API_SUCCESS,
  SIGN_UP,
  CREATE,
  USER,
  READ,
  CLEAN_USER_SERVICE
} from '../actions/actionTypes';

const initialState = {
  signupForm: {
    username: '',
    email: '',
    password: '',
    password_confirmation: ''
  },
  user: {},
  loading: false,
  email: ''
};

function user(state = initialState, action) {
  switch (action.type) {
    case `${SIGN_UP} ${CREATE} ${API_SUCCESS}`:
      return { ...state, user: action.payload, loading: false };
    case `${USER} ${READ} ${API_SUCCESS}`:
    case `${USER} ${READ} ${API_FAILURE}`:
    case `${SIGN_UP} ${CREATE} ${API_FAILURE}`:
      return { ...state, loading: false };
    case `${USER} ${READ} ${API_PENDING}`:
    case `${SIGN_UP} ${CREATE} ${API_PENDING}`:
      return { ...state, loading: true };
    case CLEAN_USER_SERVICE:
      return { ...state, user: {} };
    default:
      return { ...state };
  }
}

export default user;
