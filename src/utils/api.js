/* eslint-disable camelcase */
import getConfig from 'next/config';
import { getAuthInfo } from './auth';

let api_endpoint;
let publicRuntimeConfig = null;
const config = getConfig();

if (config) {
  publicRuntimeConfig = config.publicRuntimeConfig.NODE_ENV;
}

if (publicRuntimeConfig) {
  const {
    API_ENDPOINT
  } = config.publicRuntimeConfig;

  api_endpoint = API_ENDPOINT;
} else {
  api_endpoint = 'https://alliottechnicall.cl/v';
}

export const apiUrl = api_endpoint;

export const getApiHeaders = () => {
  const { authentication_token, email } = getAuthInfo();
  return {
    Accept: 'application/vnd.alliot.v1',
    'Content-Type': 'application/json',
    'X-Alliot-Access-Token': authentication_token,
    'X-Alliot-Email': email
  };
};
