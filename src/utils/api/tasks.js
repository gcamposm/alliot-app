/* eslint-disable import/prefer-default-export */
import { apiUrl, getApiHeaders } from '../api';

export const requestTasks = (filters = {}) => ({
  url: `${apiUrl}/tasks`,
  headers: { ...getApiHeaders(), 'Cache-Control': 'no-store' },
  method: 'GET',
  params: filters
});

export const createTask = task => ({
  url: `${apiUrl}/tasks`,
  headers: { ...getApiHeaders() },
  method: 'POST',
  data: task
});

export const editTask = task => ({
  url: `${apiUrl}/tasks/${task.id}`,
  headers: { ...getApiHeaders() },
  method: 'PUT',
  data: task
});

export const deleteTask = id => ({
  url: `${apiUrl}/tasks/${id}/archive`,
  headers: { ...getApiHeaders() },
  method: 'POST'
});
