import { apiUrl } from '../api';

export const login = form => ({
  method: 'POST',
  data: { user: form },
  url: `${apiUrl}/login`,
  headers: { 'Content-Type': 'application/json' }
});

export const signup = user => ({
  url: `${apiUrl}/sign_up`,
  method: 'POST',
  data: { user },
  headers: {
    'Content-Type': 'application/json'
  }
});
