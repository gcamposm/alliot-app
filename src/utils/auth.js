/* eslint-disable import/no-cycle */
/* eslint-disable camelcase */
import jwt from 'jsonwebtoken';
import Router from 'next/router';
import { parseCookies, setCookie, destroyCookie } from 'nookies';
import { redirectTo } from './helpers';

const secretToken =
  '53149ab8fbe1f0e92bgydf2efaa1da55c263d88c8f84a0c46e9863546786e9d797665369087fbf117fc346c3ac8982a825405721556f324d88911c7a1ec79368';

const notProtectedPaths = ['/login', '/signup'];

const decodeToken = token => {
  try {
    return jwt.verify(token, secretToken);
  } catch (error) {
    return false;
  }
};

export const getAuthInfo = (ctx = {}) => {
  const cookies = parseCookies(ctx);
  return decodeToken(cookies.ets);
};

export const destroySession = () => {
  destroyCookie({}, 'ets');
  Router.push('/login');
};

export const notProtectedPathNames = currentPath => {
  return !notProtectedPaths.includes(currentPath);
};

export const setAuth = async (data, form = {}, callback) => {
  const EncodedToken = jwt.sign(data, secretToken);
  console.log('EncodedToken', EncodedToken);

  const options = { path: '/' };
  if (form.remember) {
    options.maxAge = 60 * 60 * 24 * 3;
  }

  setCookie({}, 'ets', EncodedToken, options);
  if (typeof callback === 'function') {
    callback();
  }
};

export const redirectIfNoAuth = ctx => {
  if (ctx) {
    const { pathname } = ctx;
  
    const isAuth = getAuthInfo(ctx);
  
    if (notProtectedPathNames(pathname) && !isAuth) {
      redirectTo('/login', ctx);
      return;
    }
  
    if ((!notProtectedPathNames(pathname) || pathname === '/') && isAuth) {
      redirectTo('/tasks', ctx);
      return;
    }
  } else {
    redirectTo('/login', ctx);
    return;
  }
};
