import * as yup from 'yup';

export const string = (message = 'Campo requerido') =>
  yup
    .string()
    .required(message)
    .nullable();

export const number = (message = 'Campo requerido') => yup.number().required(message);

export const email = (message = 'Tiene que ser correo válido') =>
  yup
    .string()
    .email(message)
    .nullable();

export const sizes = () =>
  yup
    .string()
    .required('Campo requerido')
    .nullable();

export const bool = (message = 'Campo requerido') =>
  yup
    .bool()
    .required(message)
    .nullable();
