import Router from 'next/router';

export const redirectTo = (path, ctx) => {
  if (ctx && ctx.res) {
    ctx.res.writeHead(302, {
      Location: path
    });
    ctx.res.end();
  } else {
    Router.push(path);
  }
};
